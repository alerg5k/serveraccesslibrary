package com.alerg5k.lib.serveraccess;

import java.io.Serializable;

/**
 * Created by andreiz on 10/23/16.
 */
public class UserStatus implements Serializable {
    private String mStatus;
    private boolean mIsGuest;
    private int mUserId;
    private String mSessionId;
    private int mSessionExpire;

    public UserStatus() {
        mStatus = null;
        mIsGuest = true;
        mUserId = -1;
        mSessionId = null;
        mSessionExpire = -1;
    }

    public UserStatus(String status, boolean isGuest, int userId, String sessionId,
                      int sessionExpire) {
        mStatus = status;
        mIsGuest = isGuest;
        mUserId = userId;
        mSessionId = sessionId;
        mSessionExpire = sessionExpire;
    }

    public String getStatus() {
        return mStatus;
    }

    void setStatus(String status) {
        mStatus = status;
    }

    public boolean isGuest() {
        return mIsGuest;
    }

    void setIsGuest(boolean guest) {
        mIsGuest = guest;
    }

    public int getUserId() {
        return mUserId;
    }

    void setUserId(int userId) {
        mUserId = userId;
    }

    public String getSessionId() {
        return mSessionId;
    }

    void setSesionId(String sessionId) {
        mSessionId = sessionId;
    }

    public int getSessionExpire() {
        return mSessionExpire;
    }

    void setSessionExpire(int sessionExpire) {
        mSessionExpire = sessionExpire;
    }

    @Override
    public String toString() {
        String s = new String();
        s = s.concat("Status: ").concat(mStatus).concat(", ").
                concat("Guest: ").concat(mIsGuest?"True":"False").concat(", ").
                concat("User ID: ").concat(Integer.toString(mUserId)).concat(", ").
                concat("Session ID: ").concat(mSessionId).concat(", ").
                concat("Session Expire: ").concat(Integer.toString(mSessionExpire));
        return s;
    }
}
