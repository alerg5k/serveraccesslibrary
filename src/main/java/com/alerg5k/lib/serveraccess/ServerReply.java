package com.alerg5k.lib.serveraccess;

/**
 * Created by andreiz on 10/23/16.
 */
public class ServerReply {
    private String mStatus;
    private String mErrCode;
    private String mErrDescription;
    private Object mObject;

    public ServerReply() {
        mStatus = null;
        mErrCode = null;
        mErrDescription = null;
        mObject = null;
    }

    public ServerReply(String status, String errCode, String errDescription, Object obj) {
        mStatus = status;
        mErrCode = errCode;
        mErrDescription = errDescription;
        mObject = obj;
    }

    public String getStatus() {
        return mStatus;
    }

    void setStatus(String status) {
        mStatus = status;
    }

    public String getErrorCode() {
        return mErrCode;
    }

    void setErrorCode(String errCode) {
        mErrCode = errCode;
    }

    public String getErrorDescription() {
        return mErrDescription;
    }

    void setErrorDescription(String errorDescription) {
        mErrDescription = errorDescription;
    }

    public Object getObject() {
        return mObject;
    }

    void setObject(Object obj) {
        mObject = obj;
    }

    @Override
    public String toString() {
        String s = new String();
        s = s.concat("Status: ").concat(mStatus).concat(", ").
                concat("ErrCode: ").concat((mErrCode != null) ? mErrCode : "null").concat(", ").
                concat("ErrDescription: ").concat((mErrDescription != null) ? mErrDescription : "null").concat(", ").
                concat("Object: ").concat((mObject != null) ? mObject.toString() : "null");
        return s;
    }
}
