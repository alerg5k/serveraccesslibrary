package com.alerg5k.lib.serveraccess;

/**
 * Created by andreiz on 10/24/16.
 */

public interface IServerReply {
    /**
     * Callback containing an UserStatus object. Sent in response to a call to
     * {@link ServerAccess#requestUserStatus}
     *
     * @param status
     *            the get/user/status operation status
     * @param userStatus
     *            the UserStatus received from the server
     * @param errCode
     *            the get/user/status operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the get/user/status operation error description (only valid if status is not "ok")
     */
    public void onUserStatusRetrieved(String status, UserStatus userStatus, String errCode, String errDescription);

    /**
     * Callback containing an UserProfile object. Sent in response to a call to
     * {@link ServerAccess#requestUserProfile}
     *
     * @param status
     *            the get/user/profile operation status
     * @param userProfile
     *            the UserProfile received from the server
     * @param errCode
     *            the get/user/profile  operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the get/user/profile operation error description (only valid if status is not "ok")
     */
    public void onUserProfileRetrieved(String status, UserProfile userProfile, String errCode, String errDescription);

    /**
     * Callback containing an user SignIn reply. Sent in response to a call to
     * {@link ServerAccess#requestUserLogIn}
     *
     * @param status
     *            the post/user/login operation status
     * @param userId
     *            the uid of the user
     * @param errCode
     *            the post/user/login operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the post/user/login operation error description (only valid if status is not "ok")
     */
    public void onUserLogInReply(String status, int userId, String errCode, String errDescription);

    /**
     * Callback containing an user SignOut reply. Sent in response to a call to
     * {@link ServerAccess#requestUserLogOut}
     *
     * @param status
     *            the get/user/logout operation status
     * @param errCode
     *            the get/user/logout operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the get/user/logout operation error description (only valid if status is not "ok")
     */
    public void onUserLogOutReply(String status, String errCode, String errDescription);

    /**
     * Callback containing an user registration reply. Sent in response to a call to
     * {@link ServerAccess#requestUserRegistration}
     *
     * @param status
     *            the user registration operation status
     * @param errCode
     *            the user registration operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the user registration operation error description (only valid if status is not "ok")
     */
    public void onUserRegistrationReply(String status, String errCode, String errDescription);

    /**
     * Callback containing an user SignIn reply. Sent in response to a call to
     * {@link ServerAccess#requestUsernameCheck(String)}
     *
     * @param status
     *            the get/user/checkname operation status
     * @param userId
     *            the uid of the user (if found)
     * @param username
     *            the verified username
     * @param errCode
     *            the get/user/checkname operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the get/user/checkname operation error description (only valid if status is not "ok")
     */
    public void onCheckUserReply(String status, int userId, String username, String errCode, String errDescription);

    /**
     * Callback containing an user SignIn reply. Sent in response to a call to
     * {@link ServerAccess#requestEmailCheck(String)}
     *
     * @param status
     *            the get/user/checkname operation status
     * @param userId
     *            the uid of the user (if found)
     * @param email
     *            the verified email address
     * @param errCode
     *            the get/user/checkname operation error code (only valid if status is not "ok")
     * @param errDescription
     *            the get/user/checkname operation error description (only valid if status is not "ok")
     */
    public void onCheckEmailReply(String status, int userId, String email, String errCode, String errDescription);
}
