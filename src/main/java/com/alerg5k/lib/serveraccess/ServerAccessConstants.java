package com.alerg5k.lib.serveraccess;

/**
 * Created by andreiz on 10/25/16.
 */

public class ServerAccessConstants {
    public static final boolean DEBUG = true;

    public static final String BUNDLE_EXTRA_STATUS       = "status";
    public static final String BUNDLE_EXTRA_ERR_CODE     = "errCode";
    public static final String BUNDLE_EXTRA_ERR_DESC     = "errDesc";
    public static final String BUNDLE_EXTRA_USER_STATUS  = "userStatus";
    public static final String BUNDLE_EXTRA_USER_PROFILE = "userProfile";
    public static final String BUNDLE_EXTRA_USER_ID      = "userId";

    public static final String BUNDLE_EXTRA_FULLNAME     = "fullname";
    public static final String BUNDLE_EXTRA_USERNAME     = "username";
    public static final String BUNDLE_EXTRA_PASSWORD     = "password";
    public static final String BUNDLE_EXTRA_EMAIL        = "email";
    public static final String BUNDLE_EXTRA_FIRSTNAME    = "firstname";
    public static final String BUNDLE_EXTRA_LASTNAME     = "lastname";

}
