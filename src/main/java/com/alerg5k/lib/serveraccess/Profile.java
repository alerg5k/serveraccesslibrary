package com.alerg5k.lib.serveraccess;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by andreiz on 10/23/16.
 */
public class Profile implements Serializable {
    static final String MALE = "M";
    static final String FEMALE = "F";

    String mCity;
    String mCountry;
    String mPhone;
    String mGender;
    Date mBirthdate;

    Profile() {
        mCity = null;
        mCountry = null;
        mPhone = null;
        mGender = null;
        mBirthdate = null;
    }

    Profile(String city, String country, String phone, String gender, Date birthdate) {
        mCity = city;
        mCountry = country;
        mPhone = phone;
        if (MALE.equals(gender) || FEMALE.equals(gender)) {
            mGender = gender;
        }
        mBirthdate = birthdate;
    }

    public String getCity() {
        return mCity;
    }

    void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    void setCountry(String country) {
        mCountry = country;
    }

    public String getPhone() {
        return mPhone;
    }

    void setPhone(String phone) {
        mPhone = phone;
    }

    public String getGender() {
        return mGender;
    }

    void setGender(String gender) {
        mGender = gender;
    }

    public Date getBirthdate() {
        return mBirthdate;
    }

    void setBirthdate(Date birthdate) {
        mBirthdate = birthdate;
    }
}
