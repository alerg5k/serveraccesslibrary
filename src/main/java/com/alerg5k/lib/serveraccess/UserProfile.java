package com.alerg5k.lib.serveraccess;

import java.util.Date;
import java.util.List;
import java.io.Serializable;

/**
 * Created by andreiz on 10/23/16.
 */
public class UserProfile implements Serializable {
    private int mId;
    private String mStatus;
    private List<String> mErrors;
    private List<String> mGroups;
    private String mName;
    private String mUsername;
    // TODO: store password hash
    private String mPassword;
    private String mEmail;
    private Date mRegisterDate;
    private Date mLastVisit;
    private Profile mProfile;

    public UserProfile() {
        mId = -1;
        mStatus = null;
        mErrors = null;
        mGroups = null;
        mName = null;
        mUsername = null;
        mPassword = null;
        mEmail = null;
        mRegisterDate = null;
        mLastVisit = null;
        mProfile = null;
    }

    public UserProfile(int id, String status, List<String> errors, List<String> groups, String name,
                       String username, String password, String email, Date registerDate, Date lastVisit,
                       Profile profile) {
        mId = id;
        mStatus = status;
        mErrors = errors;
        mGroups = groups;
        mName = name;
        mUsername = username;
        mPassword = password;
        mEmail = email;
        mRegisterDate = registerDate;
        mLastVisit = lastVisit;
        mProfile = profile;
    }

    public UserProfile(UserProfile userProfile, String password) {
        mId = userProfile.getId();
        mStatus = userProfile.getStatus();
        mErrors = userProfile.getErrors();
        mGroups = userProfile.getGroups();
        mName = userProfile.getName();
        mUsername = userProfile.getUsername();
        mPassword = password;
        mEmail = userProfile.getEmail();
        mRegisterDate = userProfile.getRegisterDate();
        mLastVisit = userProfile.getLastVisit();
        mProfile = userProfile.getProfile();
    }

    public int getId() {
        return mId;
    }

    void setId(int id) {
        mId = id;
    }

    public String getStatus() {
        return mStatus;
    }

    void setStatus(String status) {
        mStatus = status;
    }

    public List<String> getErrors() {
        return mErrors;
    }

    void setErrors(List<String> errors) {
        mErrors = errors;
    }

    public List<String> getGroups() {
        return mGroups;
    }

    void setGroups(List<String> groups) {
        mGroups = groups;
    }

    public String getName() {
        return mName;
    }

    void setName(String name) {
        mName = name;
    }

    public String getUsername() {
        return mUsername;
    }

    void setPassword(String password) {
        mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    void setUsername(String username) {
        mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    void setEmail(String email) {
        mEmail = email;
    }

    public Date getRegisterDate() {
        return mRegisterDate;
    }

    void setRegisterDate(Date registerDate) {
        mRegisterDate = registerDate;
    }

    public Date getLastVisit() {
        return mLastVisit;
    }

    void setLastVisit(Date lastVisit) {
        mLastVisit = lastVisit;
    }

    public Profile getProfile() {
        return mProfile;
    }

    void setProfile(Profile profile) {
        mProfile = profile;
    }

    @Override
    public String toString() {
        String s = new String();
        s = s.concat("userId: ").concat(Integer.toString(mId)).concat(", ").
                concat("name: ").concat((mName != null) ? mName : "N/A").concat(", ").
                concat("username: ").concat((mUsername != null) ? mUsername : "N/A").concat(", ").
                concat("password: ").concat(((mPassword != null) ? "Password hash set!" : "Not set!")).concat(", ").
                concat("email: ").concat((mEmail != null) ? mEmail : "N/A").concat(", ").
                concat("Profile: (").concat("City: ").concat((mProfile != null && mProfile.getCity() != null) ?
                            mProfile.getCity() : "N/A").concat(", ").
                concat("Country: ").concat((mProfile != null && mProfile.getCountry() != null) ?
                            mProfile.getCountry() : "N/A").concat(", ").
                concat("Phone: ").concat((mProfile != null && mProfile.getPhone() != null) ?
                            mProfile.getPhone() : "N/A").concat(", ").
                concat("Genre: ").concat((mProfile != null && mProfile.getGender() != null) ? mProfile.getGender() : "N/A").concat(", ").
                concat("Birthdate: ").concat((mProfile != null && mProfile.getBirthdate() != null) ?
                            mProfile.getBirthdate().toString() : "N/A").concat("), ").
                concat("registerDate: ").concat((mRegisterDate != null) ? mRegisterDate.toString() : "N/A").concat(", ").
                concat("lastVisit: ").concat((mLastVisit != null) ? mLastVisit.toString() : "N/A").concat(", ").
                concat("errors: ").concat((mErrors != null) ? mErrors.toString() : "N/A").concat(", ").
                concat("groups: ").concat((mGroups != null) ? mGroups.toString() : "N/A");
        return s;
    }
}
