package com.alerg5k.lib.serveraccess;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;

import static com.alerg5k.lib.serveraccess.ServerAccessConstants.DEBUG;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_STATUS;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_ERR_CODE;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_ERR_DESC;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_USER_STATUS;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_USER_PROFILE;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_USER_ID;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_USERNAME;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_PASSWORD;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_EMAIL;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_FIRSTNAME;
import static com.alerg5k.lib.serveraccess.ServerAccessConstants.BUNDLE_EXTRA_LASTNAME;

/**
 * Created by andreiz on 10/23/16.
 */
public class ServerAccess {
    private static final String TAG = "Alerg5K.ServerAccess";

    private ServerAccessHandler mHandler = new ServerAccessHandler();
    private ArrayList<IServerReply> mReplyHandler =
            new ArrayList<IServerReply>();

    //http://alerg5k.ro/index.php/api/ get/  user/status
    //http://alerg5k.ro/index.php/api/ get/  user/logout
    //http://alerg5k.ro/index.php/api/ get/  user/profile
    //http://alerg5k.ro/index.php/api/ post/ user/login         ?username=&password=
    //http://alerg5k.ro/index.php/api/ post/ user/register      ?username=&password=&email=&firstname=&lastname=
    //http://alerg5k.ro/index.php/api/ get/  user/checkname     ?username=
    //http://alerg5k.ro/index.php/api/ get/  user/checkemail    ?email=

    private static final String ALERG5K_SERVER_HTTP  = "http://alerg5k.ro";
    private static final String ALERG5K_SERVER_HTTPS = "https://www.alerg5k.ro";
    private static final String API_INTERFACE        = "index.php/api";
    private static final String USER_STATUS          = "user/status";
    private static final String USER_PROFILE         = "user/profile";
    private static final String USER_LOGIN           = "user/login";
    private static final String USER_LOGOUT          = "user/logout";
    private static final String USER_REGISTER        = "user/register";
    private static final String USER_CHECK_NAME      = "user/checkname";
    private static final String USER_CHECK_EMAIL     = "user/checkemail";

    private static final int HTTP_GET = 0;
    private static final int HTTP_POST = 1;

    private HttpURLConnection httpConn = null;
    private HttpsURLConnection httpsConn = null;
    private SSLContext clientContext = null;
    private static final boolean useHttps = false;

    private static String mVerifyUsername = null;
    private static String mVerifyEmail = null;

    public static final String SERVER_REPLY_STATUS_OK     = "ok";
    public static final String SERVER_REPLY_STATUS_NOT_OK = "ko";
    public static final String NULL                       = "null";

    public ServerAccess() {
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        try {
            clientContext = SSLContext.getInstance("SSL");
            // TODO: do we need to implement a Key Manager ?
            clientContext.init(null, null, null);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            Log.e(TAG, e.toString());
        }
    }

    public boolean registerEventHandler(IServerReply replyHandler) {
        debug("registerEventHandler(" + replyHandler + ")");
        synchronized (mReplyHandler) {
            if (!mReplyHandler.contains(replyHandler)) {
                return mReplyHandler.add(replyHandler);
            }
        }
        return false;
    }

    public boolean unregisterEventHandler(IServerReply replyHandler) {
        debug("unregisterEventHandler(" + replyHandler + ")");
        synchronized (mReplyHandler) {
            if (mReplyHandler.contains(replyHandler)) {
                return mReplyHandler.remove(replyHandler);
            }
        }
        return false;
    }

    public void requestUserStatus() {
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_GET_USER_STATUS);
        mHandler.sendMessage(msg);
    }

    public void requestUserProfile() {
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_GET_USER_PROFILE);
        mHandler.sendMessage(msg);
    }

    public void requestUserLogIn(String username, String password) {
        // TODO: need to validate arguments
        // TODO: protect against issues like sql injection
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_LOG_IN_USER);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EXTRA_USERNAME, username);
        bundle.putString(BUNDLE_EXTRA_PASSWORD, password);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    public void requestUserRegistration(String username, String password, String email, String firstname, String lastname) {
        // TODO: need to validate arguments
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_REGISTER_USER);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EXTRA_USERNAME, username);
        bundle.putString(BUNDLE_EXTRA_PASSWORD, password);
        bundle.putString(BUNDLE_EXTRA_EMAIL, email);
        bundle.putString(BUNDLE_EXTRA_FIRSTNAME, firstname);
        bundle.putString(BUNDLE_EXTRA_LASTNAME, lastname);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    public void requestUsernameCheck(String username) {
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_CHECK_USERNAME);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EXTRA_USERNAME, username);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    public void requestEmailCheck(String email) {
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_CHECK_EMAIL);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EXTRA_EMAIL, email);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    public void requestUserLogOut() {
        // TODO: do we need to pass user id?
        Message msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_CMD_LOG_OUT_USER);
        mHandler.sendMessage(msg);
    }

    private final class ServerAccessHandler extends Handler {

        /* Command messages */
        static final int MESSAGE_CMD_BASE                   = 100;
        static final int MESSAGE_CMD_GET_USER_STATUS        = 101;
        static final int MESSAGE_CMD_GET_USER_PROFILE       = 102;
        static final int MESSAGE_CMD_REGISTER_USER          = 103;
        static final int MESSAGE_CMD_LOG_IN_USER            = 104;
        static final int MESSAGE_CMD_LOG_OUT_USER           = 105;
        static final int MESSAGE_CMD_CHECK_USERNAME         = 106;
        static final int MESSAGE_CMD_CHECK_EMAIL            = 107;
        static final int MESSAGE_CMD_MAX                    = 199;

        /* Event / Reply messages */
        static final int MESSAGE_EVT_BASE                   = 200;
        static final int MESSAGE_EVT_GET_USER_STATUS_REPLY  = 201;
        static final int MESSAGE_EVT_GET_USER_PROFILE_REPLY = 202;
        static final int MESSAGE_EVT_REGISTER_USER_REPLY    = 203;
        static final int MESSAGE_EVT_LOG_IN_USER_REPLY      = 204;
        static final int MESSAGE_EVT_LOG_OUT_USER_REPLY     = 205;
        static final int MESSAGE_EVT_USERNAME_CHECK_REPLY   = 206;
        static final int MESSAGE_EVT_EMAIL_CHECK_REPLY      = 207;
        static final int MESSAGE_EVT_MAX                    = 300;

        @Override
        public void handleMessage(Message msg) {
            String status;
            String errCode;
            String errDesc;
            String extra = new String();
            Bundle data = null;
            int userId = -1;
            debug("handleMessage() - Message: " + getMessageName(msg.what));

            if (isCommandMessage(msg.what)) {
                switch (msg.what) {
                    case MESSAGE_CMD_GET_USER_STATUS:
                        serverAccessSend(msg.what, HTTP_GET, USER_STATUS, null);
                        break;
                    case MESSAGE_CMD_GET_USER_PROFILE:
                        serverAccessSend(msg.what, HTTP_GET, USER_PROFILE, null);
                        break;
                    case MESSAGE_CMD_LOG_IN_USER:
                        data = msg.getData();
                        extra = extra.concat("username=").concat(data.getString(BUNDLE_EXTRA_USERNAME, ""));
                        extra = extra.concat("&");
                        extra = extra.concat("password=").concat(data.getString(BUNDLE_EXTRA_PASSWORD, ""));
                        serverAccessSend(msg.what, HTTP_POST, USER_LOGIN, extra);
                        break;
                    case MESSAGE_CMD_LOG_OUT_USER:
                        serverAccessSend(msg.what, HTTP_GET, USER_LOGOUT, null);
                        break;
                    case MESSAGE_CMD_REGISTER_USER:
                        data = msg.getData();
                        extra = extra.concat("username=").concat(data.getString(BUNDLE_EXTRA_USERNAME, ""));
                        extra = extra.concat("&");
                        extra = extra.concat("password=").concat(data.getString(BUNDLE_EXTRA_PASSWORD, ""));
                        extra = extra.concat("&");
                        extra = extra.concat("email=").concat(data.getString(BUNDLE_EXTRA_EMAIL, ""));
                        extra = extra.concat("&");
                        extra = extra.concat("firstname=").concat(data.getString(BUNDLE_EXTRA_FIRSTNAME, ""));
                        extra = extra.concat("&");
                        extra = extra.concat("lastname=").concat(data.getString(BUNDLE_EXTRA_LASTNAME, ""));
                        serverAccessSend(msg.what, HTTP_POST, USER_REGISTER, extra);
                        break;
                    case MESSAGE_CMD_CHECK_USERNAME:
                        data = msg.getData();
                        mVerifyUsername = data.getString(BUNDLE_EXTRA_USERNAME, "");
                        extra = extra.concat("username=").concat(mVerifyUsername);
                        serverAccessSend(msg.what, HTTP_GET, USER_CHECK_NAME, extra);
                        break;
                    case MESSAGE_CMD_CHECK_EMAIL:
                        data = msg.getData();
                        mVerifyEmail = data.getString(BUNDLE_EXTRA_EMAIL, "");
                        extra = extra.concat("email=").concat(mVerifyEmail);
                        serverAccessSend(msg.what, HTTP_GET, USER_CHECK_EMAIL, extra);
                        break;
                    default:
                        Log.e(TAG, "Unknown message: " + msg.what);
                        break;
                }
            } else if (isEventMessage(msg.what)) {
                switch (msg.what) {
                    case MESSAGE_EVT_GET_USER_STATUS_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE);
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC);
                        UserStatus userStatus = null;
                        if (isSuccess(status)) {
                            userStatus = (UserStatus) data.getSerializable(BUNDLE_EXTRA_USER_STATUS);
                        }
                        sendUserStatusReply(status, userStatus, errCode, errDesc);
                        break;
                    case MESSAGE_EVT_GET_USER_PROFILE_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE);
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC);
                        UserProfile userProfile = null;
                        if (isSuccess(status)) {
                            userProfile = (UserProfile) data.getSerializable(BUNDLE_EXTRA_USER_PROFILE);
                        }
                        sendUserProfileReply(status, userProfile, errCode, errDesc);
                        break;
                    case MESSAGE_EVT_LOG_IN_USER_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE);
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC);
                        if (isSuccess(status)) {
                            userId = data.getInt(BUNDLE_EXTRA_USER_ID);
                        }
                        sendUserLogInReply(status, userId, errCode, errDesc);
                        break;
                    case MESSAGE_EVT_LOG_OUT_USER_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE);
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC);
                        sendUserLogOutReply(status, errCode, errDesc);
                        break;
                    case MESSAGE_EVT_REGISTER_USER_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE);
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC);
                        sendUserRegistrationReply(status, errCode, errDesc);
                        break;
                    case MESSAGE_EVT_USERNAME_CHECK_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE, "");
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC, "");
                        if (isSuccess(status)) {
                            userId = data.getInt(BUNDLE_EXTRA_USER_ID);
                        }
                        sendUsernameCheckReply(status, userId, mVerifyUsername, errCode, errDesc);
                        mVerifyUsername = null;
                        break;
                    case MESSAGE_EVT_EMAIL_CHECK_REPLY:
                        data = msg.getData();
                        status = data.getString(BUNDLE_EXTRA_STATUS);
                        errCode = data.getString(BUNDLE_EXTRA_ERR_CODE, "");
                        errDesc = data.getString(BUNDLE_EXTRA_ERR_DESC, "");
                        if (isSuccess(status)) {
                            userId = data.getInt(BUNDLE_EXTRA_USER_ID);
                        }
                        sendEmailCheckReply(status, userId, mVerifyEmail, errCode, errDesc);
                        mVerifyEmail = null;
                        break;
                }
            }
        }

        private boolean isCommandMessage(int message) {
            return (message > MESSAGE_CMD_BASE && message < MESSAGE_CMD_MAX);
        }

        private boolean isEventMessage(int message) {
            return (message > MESSAGE_EVT_BASE && message < MESSAGE_EVT_MAX);
        }

        String getMessageName(int what) {
            if (isCommandMessage(what)) {
                switch (what) {
                    case MESSAGE_CMD_GET_USER_STATUS:
                        return "MESSAGE_CMD_GET_USER_STATUS";
                    case MESSAGE_CMD_GET_USER_PROFILE:
                        return "MESSAGE_CMD_GET_USER_PROFILE";
                    case MESSAGE_CMD_LOG_IN_USER:
                        return "MESSAGE_CMD_LOG_IN_USER";
                    case MESSAGE_CMD_LOG_OUT_USER:
                        return "MESSAGE_CMD_LOG_OUT_USER";
                    case MESSAGE_CMD_REGISTER_USER:
                        return "MESSAGE_CMD_REGISTER_USER";
                    case MESSAGE_CMD_CHECK_USERNAME:
                        return "MESSAGE_CMD_CHECK_USERNAME";
                    case MESSAGE_CMD_CHECK_EMAIL:
                        return "MESSAGE_CMD_CHECK_EMAIL";
                }
            } else if (isEventMessage(what)) {
                switch (what) {
                    case MESSAGE_EVT_GET_USER_STATUS_REPLY:
                        return "MESSAGE_EVT_GET_USER_STATUS_REPLY";
                    case MESSAGE_EVT_GET_USER_PROFILE_REPLY:
                        return "MESSAGE_EVT_GET_USER_PROFILE_REPLY";
                    case MESSAGE_EVT_LOG_IN_USER_REPLY:
                        return "MESSAGE_EVT_LOG_IN_USER_REPLY";
                    case MESSAGE_EVT_LOG_OUT_USER_REPLY:
                        return "MESSAGE_EVT_LOG_OUT_USER_REPLY";
                    case MESSAGE_EVT_REGISTER_USER_REPLY:
                        return "MESSAGE_EVT_REGISTER_USER_REPLY";
                    case MESSAGE_EVT_USERNAME_CHECK_REPLY:
                        return "MESSAGE_EVT_USERNAME_CHECK_REPLY";
                    case MESSAGE_EVT_EMAIL_CHECK_REPLY:
                        return "MESSAGE_EVT_EMAIL_CHECK_REPLY";
                }
            }
            return "UNKNOWN_MESSAGE";
        }
    }

    private void serverAccessSend(final int msgId, final int operation, final String request,
                                  final String extra) {
        Log.d(TAG, "Execute " + (operation == HTTP_GET ? "GET" : "POST") + ": " + request +
                ((extra != null) ? "/" + extra : ""));

        new Thread(new Runnable() {
            public void run() {
                URL url = null;
                String urlRequest = new String();

                if (useHttps) {
                    urlRequest = urlRequest.concat(ALERG5K_SERVER_HTTPS).concat("/");
                } else {
                    urlRequest = urlRequest.concat(ALERG5K_SERVER_HTTP).concat("/");
                }
                urlRequest = urlRequest.concat(API_INTERFACE).concat("/");
                if (operation == HTTP_GET) {
                    urlRequest = urlRequest.concat("get");
                } else if (operation == HTTP_POST) {
                    urlRequest = urlRequest.concat("post");
                } else {
                    Log.e(TAG, "Only GET and POST requests supported! Should never get here.");
                    return;
                }
                urlRequest = urlRequest.concat("/").concat(request);
                if (extra != null) {
                    urlRequest = urlRequest.concat("?").concat(extra);
                }
                debug("URL request: " + urlRequest);

                try {
                    url = new URL(urlRequest);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                final Object inputStreamLock = new Object();
                InputStream inputStream = null;
                int responseCode = 0;

                try {
                    if (useHttps) {
                        httpsConn = (HttpsURLConnection) url.openConnection();
                        httpsConn.setSSLSocketFactory(clientContext.getSocketFactory());

                        if (operation == HTTP_GET) {
                            httpsConn.setRequestMethod("GET");
                        } else if (operation == HTTP_POST) {
                            httpsConn.setRequestMethod("POST");
                        }
                        // Set connection timeout to 60s
                        httpsConn.setConnectTimeout(60 * 1000);
                        httpsConn.connect();
                        responseCode = httpsConn.getResponseCode();
                    } else {
                        httpConn = (HttpURLConnection) url.openConnection();
                        if (operation == HTTP_GET) {
                            httpConn.setRequestMethod("GET");
                        } else {
                            httpConn.setRequestMethod("POST");
                        }
                        // Set connection timeout to 60s
                        httpConn.setConnectTimeout(60 * 1000);
                        httpConn.connect();
                        responseCode = httpConn.getResponseCode();
                    }
                    debug("ResponseCode=" + responseCode);

                    try {
                        synchronized (inputStreamLock) {
                            if (useHttps)
                                inputStream = httpsConn.getInputStream();
                            else
                                inputStream = httpConn.getInputStream();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Device does not have internet connectivity!");
                        e.printStackTrace();
                        throw e;
                    }
                    parseResponse(msgId, inputStream, null);
                } catch (SocketTimeoutException e) {
                    // server communication timeout
                    parseResponse(msgId, null, e);
                } catch (NullPointerException | IOException e) {
                    // UnknownHostException (thrown when there is no Internet access or server is down)
                    // is a subclass of IOException, so we capture the generic error here
                    parseResponse(msgId, null, e);
                } finally {
                    if (httpsConn != null) {
                        httpsConn.disconnect();
                    }
                    if (httpConn != null) {
                        httpConn.disconnect();
                    }
                    if (inputStream != null) {
                        try {
                            synchronized (inputStreamLock) {
                                inputStream.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    private void parseResponse(int msgId, InputStream inputStream, Exception ex) {
        debug("Parse JSON response for msg: " + mHandler.getMessageName(msgId));
        Message msg = null;
        ServerReply reply = null;
        Bundle data = null;
        int userId = -1;

        switch (msgId) {
            case ServerAccessHandler.MESSAGE_CMD_GET_USER_STATUS:
                UserStatus userStatus = null;
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserStatusJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_GET_USER_STATUS_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                if (isSuccess(reply.getStatus())) {
                    userStatus = (UserStatus) reply.getObject();
                    data.putSerializable(BUNDLE_EXTRA_USER_STATUS, userStatus);
                }
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_GET_USER_PROFILE:
                UserProfile userProfile = null;
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserProfileJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_GET_USER_PROFILE_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                if (isSuccess(reply.getStatus())) {
                    userProfile = (UserProfile) reply.getObject();
                    data.putSerializable(BUNDLE_EXTRA_USER_PROFILE, userProfile);
                }
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_REGISTER_USER:
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserOperationJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_REGISTER_USER_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_LOG_IN_USER:
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserOperationJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_LOG_IN_USER_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                if (isSuccess(reply.getStatus())) {
                    userId = ((Integer) reply.getObject()).intValue();
                    data.putInt(BUNDLE_EXTRA_USER_ID, userId);
                }
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_LOG_OUT_USER:
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserOperationJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_LOG_OUT_USER_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_CHECK_USERNAME:
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserOperationJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_USERNAME_CHECK_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                if (isSuccess(reply.getStatus())) {
                    userId = ((Integer) reply.getObject()).intValue();
                    data.putInt(BUNDLE_EXTRA_USER_ID, userId);
                }
                mHandler.sendMessage(msg);
                break;
            case ServerAccessHandler.MESSAGE_CMD_CHECK_EMAIL:
                if (inputStream != null && ex == null) {
                    try {
                        reply = readUserOperationJsonStream(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    reply = new ServerReply();
                    reply.setStatus(SERVER_REPLY_STATUS_NOT_OK);
                    reply.setErrorCode(ex.getCause().getMessage());
                    reply.setErrorDescription(ex.getLocalizedMessage());
                }
                msg = mHandler.obtainMessage(ServerAccessHandler.MESSAGE_EVT_EMAIL_CHECK_REPLY);
                data = msg.getData();
                data.putString(BUNDLE_EXTRA_STATUS, reply.getStatus());
                data.putString(BUNDLE_EXTRA_ERR_CODE, reply.getErrorCode());
                data.putString(BUNDLE_EXTRA_ERR_DESC, reply.getErrorDescription());
                if (isSuccess(reply.getStatus())) {
                    userId = ((Integer) reply.getObject()).intValue();
                    data.putInt(BUNDLE_EXTRA_USER_ID, userId);
                }
                mHandler.sendMessage(msg);
                break;
            default:
                Log.e(TAG, "Invalid message ID! Should never get here.");
                break;
        }
    }

    public static boolean isSuccess(String status) {
        return SERVER_REPLY_STATUS_OK.equals(status);
    }

    private ServerReply readUserStatusJsonStream(InputStream in) throws IOException {
        ServerReply serverReply = new ServerReply();
        UserStatus userStatus = null;
        String status = null;
        boolean isGuest = false;
        int userId = -1;
        String sessionId = null;
        int sessionExpire = -1;

        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        // {"status":"ok","is_guest":1,"user_id":0,"session_id":"s35qt078g68ltukioa665du802","session_expire":900}
        // {"status":"ok","is_guest":0,"user_id":"669","session_id":"h5es75hdnjrg2d80ushtbcp517","session_expire":900}

        reader.beginObject();
        while (reader.hasNext()) {
            String tag = reader.nextName();
            if (tag.equals("status")) {
                status = reader.nextString();
                serverReply.setStatus(status);
            } else if (tag.equals("is_guest")) {
                isGuest = (reader.nextInt() == 1);
            } else if (tag.equals("user_id")) {
                userId = reader.nextInt();
            } else if (tag.equals("session_id")) {
                sessionId = reader.nextString();
            } else if (tag.equals("session_expire")) {
                sessionExpire = reader.nextInt();
            } else {
                debug("Found unexpected tag: " + tag);
                reader.skipValue();
            }
        }
        reader.endObject();
        userStatus = new UserStatus(status, isGuest, userId, sessionId, sessionExpire);
        serverReply.setObject(userStatus);
        return serverReply;
    }

    private ServerReply readUserProfileJsonStream(InputStream in) throws IOException {
        ServerReply serverReply = new ServerReply();
        UserProfile userProfile = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int id = -1;
        String status = null;
        List<String> errors = new ArrayList<String>();
        List<String> groups = new ArrayList<String>();
        String name = null;
        String username = null;
        String email = null;
        Date registerDate = null;
        Date lastVisit = null;
        Profile profile = null;
        String city = null;
        String country = null;
        String phone = null;
        String gender = null;
        Date birthdate = null;

        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        // {"status":"ko","error_code":"USR_UNL","error_description":"User not logged in"}

        // {"status":"ok","_errors":[],"groups":{"2":"2"},"id":"669","name":"Gina Manta",
        //  "username":"r2","email":"r2@gmail.com","block":"0","sendEmail":"0",
        //  "registerDate":"2016-04-15 13:57:46","lastvisitDate":"2016-10-23 20:52:53",
        //  "activation":"","params":{"admin_style":"","admin_language":"","language":"",
        //  "editor":"","helpsite":"","timezone":""},"lastResetTime":"0000-00-00 00:00:00",
        //  "resetCount":"0","otpKey":"","otep":"","requireReset":"0","tags":{"typeAlias":null,"tags":""},
        //  "profile":{"city":"","country":"","phone":"","gender":"F","dob":"1984-04-10 00:00:00"}}

        reader.beginObject();
        while (reader.hasNext()) {
            String tag = reader.nextName();
            if (tag.equals("status")) {
                status = reader.nextString();
                serverReply.setStatus(status);
                if ("ko".equals(status)) {
                    // Failed to retrieve the profile data
                    serverReply.setStatus(status);
                    String errCode = null;
                    String errDescription = null;
                    tag = reader.nextName();
                    if (tag.equals("error_code")) {
                        errCode = reader.nextString();
                        serverReply.setErrorCode(errCode);
                    }
                    tag = reader.nextName();
                    if (tag.equals("error_description")) {
                        errDescription = reader.nextString();
                        serverReply.setErrorDescription(errDescription);
                    }
                    reader.endObject();
                    return serverReply;
                }
            } else if (tag.equals("name")) {
                name = reader.nextString();
            } else if (tag.equals("id")) {
                id = reader.nextInt();
            } else if (tag.equals("_errors")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    errors.add(reader.nextString());
                }
                reader.endArray();
            } else if (tag.equals("groups")) {
                reader.beginObject();
                while (reader.hasNext()) {
                    // Groups are made from <name>:<value> pairs
                    tag = reader.nextName();
                    groups.add(reader.nextString());
                }
                reader.endObject();
            } else if (tag.equals("username")) {
                username = reader.nextString();
            } else if (tag.equals("email")) {
                email = reader.nextString();
            } else if (tag.equals("registerDate")) {
                try {
                    registerDate = format.parse(reader.nextString());
                } catch (ParseException e) {
                    Log.e(TAG, e.toString());
                }
            } else if (tag.equals("lastvisitDate")) {
                try {
                    lastVisit = format.parse(reader.nextString());
                } catch (ParseException e) {
                    Log.e(TAG, e.toString());
                }
            } else if (tag.equals("profile")) {
                reader.beginObject();
                while (reader.hasNext()) {
                    tag = reader.nextName();
                    if (tag.contentEquals("city")) {
                        city = reader.nextString();
                    } else if (tag.contentEquals("country")) {
                        country = reader.nextString();
                    } else if (tag.contentEquals("phone")) {
                        phone = reader.nextString();
                    } else if (tag.contentEquals("gender")) {
                        gender = reader.nextString();
                    } else if (tag.contentEquals("dob")) {
                        try {
                            birthdate = format.parse(reader.nextString());
                        } catch (ParseException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                }
                reader.endObject();
            } else {
                // Log.i(TAG, "Ignoring tag: " + tag);
                reader.skipValue();
            }
        }
        reader.endObject();
        userProfile = new UserProfile(id, status, errors, groups, name, username, null, email,
                registerDate, lastVisit, new Profile(city, country, phone, gender, birthdate));
        serverReply.setObject(userProfile);
        return serverReply;
    }

    private ServerReply readUserOperationJsonStream(InputStream in) throws IOException {
        ServerReply serverReply = new ServerReply();
        String status = null;
        String userId = null;
        String username = null;
        String sessionId = null;
        int id = -1;

        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        // {"status":"ok","userid":"669","username":"r2","session_id":"h5es75hdnjrg2d80ushtbcp517"}
        // {"status":"ko","error_code":"REQ_RUN","error_description":"Request unknown"}
        // {"status":"ko","error_code":"USR_UNE","error_description":"User not enabled"}
        // {"status":"ko","error_code":"USR_LOF","error_description":"Logout failed"}
        // {"status":"ko","error_code":"USR_UNR","error_description":"Username required"}
        // {"status":"ok","userid":"669"}
        // {"status":"ok","userid":"null"}
        // {"status":"ok"}

        reader.beginObject();
        while (reader.hasNext()) {
            String tag = reader.nextName();
            if (tag.equals("status")) {
                status = reader.nextString();
                serverReply.setStatus(status);
                if ("ko".equals(status)) {
                    // Failed operation Log In / Log Out / Register user
                    serverReply.setStatus(status);
                    String errCode = null;
                    String errDescription = null;
                    tag = reader.nextName();
                    if (tag.equals("error_code")) {
                        errCode = reader.nextString();
                        serverReply.setErrorCode(errCode);
                    }
                    tag = reader.nextName();
                    if (tag.equals("error_description")) {
                        errDescription = reader.nextString();
                        serverReply.setErrorDescription(errDescription);
                    }
                    reader.endObject();
                    return serverReply;
                }
            } else if (tag.equals("userid")) {
                userId = reader.nextString();
                if (!NULL.equals(userId)) {
                    id = new Integer(userId);
                }
                serverReply.setObject(id);
            } else if (tag.equals("username")) {
                username = reader.nextString();
                // Not used for now
            } else if (tag.equals("session_id")) {
                sessionId = reader.nextString();
                // Not used for now
            } else {
                debug("Found unexpected tag: " + tag);
                reader.skipValue();
            }
        }
        reader.endObject();
        return serverReply;
    }

    private void sendUserStatusReply(String status, UserStatus userStatus, String errCode, String errDescription) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onUserStatusRetrieved(status, userStatus, errCode, errDescription);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUserStatusReply: ", t);
                    }
                }
            }
        }
    }

    private void sendUserProfileReply(String status, UserProfile userProfile, String errCode, String errDescription) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onUserProfileRetrieved(status, userProfile, errCode, errDescription);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUserProfileReply: ", t);
                    }
                }
            }
        }
    }

    private void sendUserLogInReply(String status, int userId, String errCode, String errDescription) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onUserLogInReply(status, userId, errCode, errDescription);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUserLogInReply: ", t);
                    }
                }
            }
        }
    }

    private void sendUserLogOutReply(String status, String errCode, String errDescription) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onUserLogOutReply(status, errCode, errDescription);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUserLogOutReply: ", t);
                    }
                }
            }
        }
    }

    private void sendUserRegistrationReply(String status, String errCode, String errDescription) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onUserRegistrationReply(status, errCode, errDescription);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUserProfileReply: ", t);
                    }
                }
            }
        }
    }

    private void sendUsernameCheckReply(String status, int userId, String username, String errCode, String errDesc) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onCheckUserReply(status, userId, username, errCode, errDesc);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendUsernameCheckReply: ", t);
                    }
                }
            }
        }
    }

    private void sendEmailCheckReply(String status, int userId, String email,  String errCode, String errDesc) {
        synchronized (mReplyHandler) {
            int n = mReplyHandler.size();
            for (int i = 0; i < n; i++) {
                if (mReplyHandler.get(i) != null) {
                    try {
                        mReplyHandler.get(i).onCheckEmailReply(status, userId, email, errCode, errDesc);
                    } catch (Throwable t) {
                        Log.e(TAG, "sendEmailCheckReply: ", t);
                    }
                }
            }
        }
    }

    private void debug(String debug) {
        if (DEBUG)
            Log.d(TAG, debug);
    }
}